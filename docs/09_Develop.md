# Developing on the client

You want to work on GeoVisio and offer bug fixes or new features ? That's awesome ! 🤩

Here are some inputs about working with GeoVisio client code.

If something seems missing or incomplete, don't hesitate to contact us by [email](panieravide@riseup.net) or using [an issue](https://gitlab.com/geovisio/web-viewer/-/issues). We really want GeoVisio to be a collaborative project, so everyone is welcome (see our [code of conduct](../CODE_OF_CONDUCT.md)).

__Contents__

[[_TOC_]]


## Architecture

Entry code is located in:

- `src/Viewer.js` file for classic Viewer
- `src/StandaloneMap.js` file for standalone Map viewer

The library is a combination of various other libraries:

- [Photo Sphere Viewer](https://github.com/mistic100/Photo-Sphere-Viewer), for displaying classic and 360° pictures
- [Maplibre GL JS](https://github.com/maplibre/maplibre-gl-js), for displaying the map which shows sequences and pictures location
- [JS Library Boilerplate](https://github.com/hodgef/js-library-boilerplate), for having a ready-to-use development toolbox


## Testing

We're trying to make GeoVisio as reliable and secure as possible. To ensure this, we rely heavily on code testing. A variety of testing tools is made available:

* `npm start` : launches a dev web server on [localhost:3000](http://localhost:3000)
* `npm run test` : unit testing
* `npm run lint` : syntax checks
* `npm run coverage` : amount of tested code

If you're working on bug fixes or new features, please __make sure to add appropriate tests__ to keep GeoVisio level of quality.


## Make a release

See [dedicated documentation](./90_Releases.md).

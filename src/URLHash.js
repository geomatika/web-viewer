const UPDATE_HASH_EVENTS = [
	"view-rotated", "picture-loaded", "focus-changed",
];

/**
 * Updates the URL hash with various viewer information.
 * This doesn't handle the "map" parameter, which is managed by MapLibre GL JS
 *
 * Based on https://github.com/maplibre/maplibre-gl-js/blob/main/src/ui/hash.ts
 *
 * @returns {URLHash} `this`
 *
 * @private
 */
class URLHash extends EventTarget {
	/**
	 *
	 * @param {import('./Viewer').Viewer} viewer
	 */
	constructor(viewer) {
		super();
		this._viewer = viewer;
		this._delay = null;
		this._hashChangeHandler = this._onHashChange.bind(this);
		window.addEventListener("hashchange", this._hashChangeHandler, false);
		UPDATE_HASH_EVENTS.forEach(e => this._viewer.addEventListener(e, this._updateHash.bind(this)));
	}

	/**
	 * Ends all form of life in this object.
	 */
	destroy() {
		window.removeEventListener("hashchange", this._hashChangeHandler);
		delete this._hashChangeHandler;
		UPDATE_HASH_EVENTS.forEach(e => this._viewer.removeEventListener(e, this._updateHash));
		delete this._viewer;
	}

	/**
	 * Get the hash string with current psv parameters
	 * @return {string} The hash, starting with #
	 */
	getHashString() {
		let hash = "";
		let hashParts = {};

		if(typeof this._viewer.getTransitionDuration() == "number") {
			hashParts.speed = this._viewer.getTransitionDuration();
		}

		const picMeta = this._viewer.getPictureMetadata();
		if (picMeta) {
			hashParts.pic = picMeta.id;
			hashParts.xyz = this._getXyzHashString();
		}

		Object.entries(hashParts)
			.sort((a,b) => a[0].localeCompare(b[0]))
			.forEach(entry => {
				let [ hashName, value ] = entry;
				let found = false;
				const parts = hash.split("&").map(part => {
					const key = part.split("=")[0];
					if (key === hashName) {
						found = true;
						return `${key}=${value}`;
					}
					return part;
				}).filter(a => a);
				if (!found) {
					parts.push(`${hashName}=${value}`);
				}
				hash = `${parts.join("&")}`;
			});

		return `#${hash}`.replace(/^#+/, "#");
	}

	/**
	 * Transforms window.location.hash into key->value object
	 * @return {object} Key-value read from hash
	 * @private
	 */
	_getCurrentHash() {
		// Get the current hash from location, stripped from its number sign
		const hash = window.location.hash.replace("#", "");

		// Split the parameter-styled hash into parts and find the value we need
		let keyvals = {};
		hash.split("&").map(
			part => part.split("=")
		)
			.filter(part => part[0] !== undefined && part[0].length > 0)
			.forEach(part => {
				keyvals[part[0]] = part[1];
			});

		return keyvals;
	}

	/**
	 * Get PSV view position as string
	 * @returns {string} x/y/z
	 * @private
	 */
	_getXyzHashString() {
		const xyz = this._viewer.getXYZ();
		const x = xyz.x.toFixed(2),
			y = xyz.y.toFixed(2),
			z = Math.round(xyz.z || 0);
		return `${x}/${y}/${z}`;
	}

	/**
	 * Updates map and PSV according to current hash values
	 * @private
	 */
	_onHashChange() {
		const vals = this._getCurrentHash();

		// Restore selected picture
		if(vals.pic) {
			this._viewer.goToPicture(vals.pic);
		}

		// Change speed
		if(vals.speed) {
			this._viewer.setTransitionDuration(vals.speed);
		}

		// Change xyz position
		if(vals.xyz) {
			const coords = this.getXyzOptionsFromHashString(vals.xyz);
			this._viewer.setXYZ(coords.x, coords.y, coords.z);
		}
	}

	/**
	 * Extracts from string xyz position
	 * @param {string} str The xyz position as hash string
	 * @returns {object} { x, y, z }
	 */
	getXyzOptionsFromHashString(str) {
		const loc = str.split("/");
		if (loc.length === 3 && !loc.some(v => isNaN(v))) {
			const res = {
				x: +loc[0],
				y: +loc[1],
				z: +loc[2]
			};

			return res;
		}
		else { return null; }
	}

	/**
	 * Changes the URL hash using current viewer parameters
	 * @private
	 */
	_updateHash() {
		if(this._delay) {
			clearTimeout(this._delay);
			this._delay = null;
		}

		this._delay = setTimeout(() => {
			// Replace if already present, else append the updated hash string
			const location = window.location.href.replace(/(#.+)?$/, this.getHashString());
			try {
				window.history.replaceState(window.history.state, null, location);
				const event = new CustomEvent("url-changed", { detail: {url: location}});
				this.dispatchEvent(event);
			} catch (SecurityError) {
				// IE11 does not allow this if the page is within an iframe created
				// with iframe.contentWindow.document.write(...).
				// https://github.com/mapbox/mapbox-gl-js/issues/7410
			}
		}, 500);
	}

}

export default URLHash;

import "./css/Widgets.css";
import { icon } from "@fortawesome/fontawesome-svg-core";
import { PSV_ANIM_DURATION, PSV_ZOOM_DELTA, PIC_MAX_STAY_DURATION } from "./Viewer";
import {
	faChevronUp, faChevronLeft, faChevronDown, faChevronRight,
	faPlus, faMinus, faShareFromSquare, faLink, faMap,
	faAngleDown, faPanorama, faPlay, faBackwardStep,
	faForwardStep, faPause, faCircleDot, faCheck, faPen,
	faSatelliteDish, faEllipsisVertical, faRocket, faTractor, faSun
} from "@fortawesome/free-solid-svg-icons";

/**
 * Handles all map/viewer buttons visible on UI.
 * Also handles switch between map and viewer, and responsiveness.
 *
 * @private
 */
export default class Widgets {
	/**
	 * @param {import('./Viewer').Viewer} viewer The GeoVisio viewer
	 * @param {object} [options] Widgets options
	 * @param {string} [options.editIdUrl] Edit with iD URL
	 * @param {string|Element} [options.customWidget] A user-defined widget to add
	 */
	constructor(viewer, options = {}) {
		// Set default options
		if(options == null) { options = {}; }
		if(options.editIdUrl == null) { options.editIdUrl = "https://www.openstreetmap.org/edit"; }

		this._viewer = viewer;
		this._t = this._viewer._translations;
		this._options = options;

		// Create widgets "corners"
		this._corners = {};
		const components = ["main"];
		const cornerSpace = ["top", "bottom"];
		const corners = ["left", "middle", "right"];
		for(let cp of components) {
			for(let cs of cornerSpace) {
				const csDom = document.createElement("div");
				csDom.id = `gvs-corner-${cp}-${cs}`;
				csDom.classList.add("gvs-corner-space");

				for(let cn of corners) {
					const corner = document.createElement("div");
					corner.id = `${csDom.id}-${cn}`;
					corner.classList.add("gvs-corner");
					this._corners[`${cp}-${cs}-${cn}`] = corner;
					csDom.appendChild(corner);
				}

				if(cp == "main") { this._viewer.mainContainer.appendChild(csDom); }
				else if(cp == "mini") { this._viewer.miniContainer.appendChild(csDom); }
			}
		}

		if(!this._viewer.isHeightSmall()) { this._initWidgetMove(); }
		this._initWidgetZoom();
		this._initWidgetPlayer();
		this._initWidgetLegend();

		this._initWidgetShare();

		// Custom widget provided by user
		if(options.customWidget) {
			const corner = this._corners["main-bottom-right"];

			switch(typeof options.customWidget) {
			case "string":
				for(let e of new DOMParser().parseFromString(options.customWidget, "text/html").body.children) {
					corner.appendChild(e);
				}
				break;

			case "object":
				if(Array.isArray(options.customWidget)) {
					options.customWidget.forEach(e => corner.appendChild(e));
				}
				else {
					corner.appendChild(options.customWidget);
				}
				break;
			}
		}
	}

	/**
	 * Ends all form of life in this object.
	 */
	destroy() {
		Object.values(this._corners).forEach(e => e.remove());
		delete this._corners;
		delete this._t;
		delete this._viewer;
	}

	/**
	 * Creates a new button, already styled
	 * @param {string} id The component ID
	 * @param {string|Element} content The text content
	 * @param {string} [title] A title label on overlay
	 * @param {string[]} [classes] List of CSS classes to add
	 * @returns {Element} The created button
	 * @private
	 */
	_createButton(id, content = null, title = null, classes = []) {
		const btn = document.createElement("button");
		if(content) {
			if(content instanceof HTMLElement || content instanceof Node) {
				btn.appendChild(content);
			}
			else {
				btn.innerHTML = content;
			}
		}
		btn.id = id;
		if(Array.isArray(classes)) {
			classes = classes.filter(c => c != null && c.length > 0);
		}
		btn.classList.add("gvs-btn", "gvs-widget-bg", ...classes);
		if(title) { btn.title = title; }
		return btn;
	}

	/**
	 * Creates a panel associated to a button
	 * @param {Element} btn The button to associate to
	 * @param {Element[]} [elements] DOM elements to append into
	 * @param {str[]} [classes] CSS classes to add
	 * @returns {Element} The created panel
	 * @private
	 */
	_createPanel(btn, elements = [], classes = []) {
		const panel = document.createElement("div");
		panel.id = btn.id + "-panel";
		if(Array.isArray(classes)) {
			classes = classes.filter(c => c != null && c.length > 0);
		}
		panel.classList.add("gvs-panel", "gvs-widget-bg", "gvs-hidden", ...classes);
		for(let e of elements) {
			panel.appendChild(e);
		}

		const togglePanel = () => {
			panel.classList.toggle("gvs-hidden");
		};

		btn.addEventListener("click", togglePanel);
		btn.addEventListener("hover", togglePanel);

		return panel;
	}

	/**
	 * Creates a new group of elements, already styled
	 * @param {str} id
	 * @param {str} position (format: component-corner, with component = main/mini, and corner = top-left, top-right, top, bottom-left, bottom, bottom-right)
	 * @param {Element[]} [elements] The children elements to add
	 * @param {str[]} [classes] The CSS classes to add
	 * @returns {Element} The created group
	 * @private
	 */
	_createGroup(id, position, elements = [], classes = []) {
		const group = document.createElement("div");
		group.id = id;
		if(Array.isArray(classes)) {
			classes = classes.filter(c => c != null && c.length > 0);
		}
		group.classList.add("gvs-group", ...classes);
		for(let e of elements) {
			group.appendChild(e);
		}
		this._corners[position].appendChild(group);
		return group;
	}

	/**
	 * Make a button usable
	 * @param {Element} btn
	 */
	_enableButton(btn) {
		btn.removeAttribute("disabled");
	}

	/**
	 * Make a button unusable
	 * @param {Element} btn
	 */
	_disableButton(btn) {
		btn.setAttribute("disabled", "");
	}

	/**
	 * Transform Font Awesome icon definition into HTML element
	 * @param {import("@fortawesome/free-solid-svg-icons").IconDefinition} i The icon to use
	 * @param {object} [o] [FontAwesome icon parameters](https://origin.fontawesome.com/docs/apis/javascript/methods#icon-icondefinition-params)
	 * @returns {Element} HTML element
	 * @private
	 */
	_fa(i, o) {
		return icon(i, o).node[0];
	}

	/**
	 * Transform Font Awesome icon definition into HTML text
	 * @param {import("@fortawesome/free-solid-svg-icons").IconDefinition} i The icon to use
	 * @param {object} [o] [FontAwesome icon parameters](https://origin.fontawesome.com/docs/apis/javascript/methods#icon-icondefinition-params)
	 * @returns {string} HTML element as text
	 * @private
	 */
	_fat(i, o) {
		return icon(i, o).html[0];
	}

	/**
	 * Creates the move buttons group
	 * @private
	 */
	_initWidgetMove() {
		// Presentation
		const btnMoveUp = this._createButton("gvs-move-up", this._fa(faChevronUp), this._t.gvs.moveUp);
		const btnMoveDown = this._createButton("gvs-move-down", this._fa(faChevronDown), this._t.gvs.moveDown);
		const btnMoveLeft = this._createButton("gvs-move-left", this._fa(faChevronLeft), this._t.gvs.moveLeft);
		const btnMoveRight = this._createButton("gvs-move-right", this._fa(faChevronRight), this._t.gvs.moveRight);
		const btnMoveMiddle = this._createButton("gvs-move-middle", this._fa(faCircleDot, {transform: {size: 12}}), this._t.gvs.btnMoveMiddle);
		this._createGroup(
			"gvs-widget-move",
			"main-top-right",
			[btnMoveUp, btnMoveLeft, btnMoveMiddle, btnMoveRight, btnMoveDown],
			["gvs-widget-bg"]
		);

		btnMoveUp.addEventListener("click", () => this._viewer.moveUp());
		btnMoveLeft.addEventListener("click", () => this._viewer.moveLeft());
		btnMoveDown.addEventListener("click", () => this._viewer.moveDown());
		btnMoveRight.addEventListener("click", () => this._viewer.moveRight());
		btnMoveMiddle.addEventListener("click", () => this._viewer.moveCenter());
	}
	/**
	 * Creates the zoom buttons group
	 * @private
	 */
	_initWidgetZoom() {
		this._lastWantedZoom = this._viewer.psv.getZoomLevel();

		// Presentation
		const btnZoomIn = this._createButton("gvs-zoom-in", this._fa(faPlus), this._t.gvs.zoomIn);
		const btnZoomOut = this._createButton("gvs-zoom-out", this._fa(faMinus), this._t.gvs.zoomOut);
		this._createGroup("gvs-widget-zoom", "main-top-right", [btnZoomIn, btnZoomOut], ["gvs-group-vertical"]);

		// Events
		const zoomFct = (e, zoomIn) => {
			if(this._viewer.lastPsvAnim) { this._viewer.lastPsvAnim.cancel(); }
			const goToZoom = zoomIn ?
				Math.min(100, this._lastWantedZoom + PSV_ZOOM_DELTA)
				: Math.max(0, this._lastWantedZoom - PSV_ZOOM_DELTA);
			this._viewer.lastPsvAnim = this._viewer.psv.animate({
				speed: PSV_ANIM_DURATION,
				zoom: goToZoom
			});
			this._lastWantedZoom = goToZoom;
		};

		btnZoomIn.addEventListener("click", e => zoomFct(e, true));
		btnZoomOut.addEventListener("click", e => zoomFct(e, false));
	}

	/**
	 * Creates play/pause/next/prev picture buttons
	 * @private
	 */
	_initWidgetPlayer() {
		// Presentation
		const btnPlayerPrev = this._createButton("gvs-player-prev", this._fa(faBackwardStep), this._t.gvs.sequence_prev);
		const btnPlayerPlay = this._createButton("gvs-player-play");
		const btnPlayerNext = this._createButton("gvs-player-next", this._fa(faForwardStep), this._t.gvs.sequence_next);
		const btnPlayerMore = this._createButton("gvs-player-more", this._fa(faEllipsisVertical), this._t.gvs.sequence_more, ["gvs-xs-hidden"]);

		// Panel for more options
		const pnlOpts = this._createPanel(btnPlayerMore, [], ["gvs-player-options", "gvs-input-btn"]);
		pnlOpts.innerHTML = `
			<div class="gvs-input-range" title="${this._t.gvs.sequence_speed}">
				${this._fat(faTractor)}
				<input
					id="gvs-player-speed"
					type="range" name="speed"
					min="0" max="${PIC_MAX_STAY_DURATION}"
					value="${PIC_MAX_STAY_DURATION - this._viewer._transitionDuration}"
					title="${this._t.gvs.sequence_speed}"
					style="width: 100%;" />
				${this._fat(faRocket)}
			</div>
			<button title="${this._t.gvs.contrast}" id="gvs-player-contrast">
				${this._fat(faSun)}
			</button>
		`;

		// Group widget
		const grpPlayer = this._createGroup(
			"gvs-widget-player", "main-top-middle",
			[btnPlayerPrev, btnPlayerPlay, btnPlayerNext].concat([pnlOpts, btnPlayerMore]),
			["gvs-group-horizontal", "gvs-only-psv", this._viewer.getPictureMetadata() ? "" : "gvs-hidden"]
		);

		// Toggle state of play button
		const toggleBtnPlay = (isPlaying) => {
			btnPlayerPlay.innerHTML = isPlaying ? this._fat(faPause) : this._fat(faPlay);
			btnPlayerPlay.title = isPlaying ? this._t.gvs.sequence_pause : this._t.gvs.sequence_play;
		};
		toggleBtnPlay(false);

		// Listening to viewer events
		this._viewer.addEventListener("sequence-playing", () => toggleBtnPlay(true));
		this._viewer.addEventListener("sequence-stopped", () => toggleBtnPlay(false));
		this._viewer.addEventListener("picture-loaded", () => grpPlayer.classList.remove("gvs-hidden"), { once: true });
		this._viewer.addEventListener("picture-loaded", () => {
			if(this._viewer.getPictureMetadata()?.sequence?.prevPic != null) { this._enableButton(btnPlayerPrev); }
			else { this._disableButton(btnPlayerPrev); }

			if(this._viewer.getPictureMetadata()?.sequence?.nextPic != null) {
				this._enableButton(btnPlayerNext);
				this._enableButton(btnPlayerPlay);
			}
			else {
				this._disableButton(btnPlayerNext);
				this._disableButton(btnPlayerPlay);
			}
		});

		const btnPlayerSpeed = pnlOpts.children[0].children[1];

		this._viewer.addEventListener("transition-duration-changed", e => {
			btnPlayerSpeed.value = PIC_MAX_STAY_DURATION - e.detail.value;
		});

		btnPlayerSpeed.addEventListener("change", e => {
			const newSpeed = PIC_MAX_STAY_DURATION - e.target.value;
			this._viewer.setTransitionDuration(newSpeed);
		});

		// Buttons events
		btnPlayerPrev.addEventListener("click", () => this._viewer.goToPrevPicture());
		btnPlayerNext.addEventListener("click", () => this._viewer.goToNextPicture());

		btnPlayerPlay.addEventListener("click", () => {
			if(this._viewer.isSequencePlaying()) {
				toggleBtnPlay(false);
				this._viewer.stopSequence();
			}
			else {
				toggleBtnPlay(true);
				this._viewer.playSequence();
			}
		});

		const btnPlayerContrast = document.getElementById("gvs-player-contrast");
		if(btnPlayerContrast) {
			btnPlayerContrast.addEventListener("click", () => {
				if(btnPlayerContrast.classList.contains("gvs-btn-active")) {
					btnPlayerContrast.classList.remove("gvs-btn-active");
					this._viewer.setPictureHigherContrast(false);
				}
				else {
					btnPlayerContrast.classList.add("gvs-btn-active");
					this._viewer.setPictureHigherContrast(true);
				}
			});
		}
	}

	/**
	 * Creates legend block
	 * @private
	 */
	_initWidgetLegend() {
		// Presentation (main widget)
		const mainLegend = this._createGroup(
			"gvs-widget-legend","main-bottom-left",
			[],
			["gvs-widget-bg"]
		);

		const toggleLegend = () => {
			let picLegend = "&copy; <a href='https://gitlab.com/geovisio/web-viewer/' target='_blank'>GeoVisio</a>";

			// Picture legend based on current picture metadata
			const picMeta = this._viewer.getPictureMetadata()?.caption;
			if(picMeta) {
				picLegend = "";
				if(picMeta.producer) {
					picLegend += `<span style="font-weight: bold">&copy; ${picMeta.producer}</span>`;
				}
				if(picMeta.date) {
					if(picMeta.producer) { picLegend += "&nbsp;-&nbsp;"; }
					picLegend += picMeta.date.toLocaleDateString(undefined, { year: "numeric", month: "long", day: "numeric" });
				}
			}

			mainLegend.innerHTML = picLegend;
		};

		toggleLegend();

		// Listening to viewer events
		this._viewer.addEventListener("picture-loaded", () => toggleLegend());
	}

	/**
	 * Creates expand/reduce mini component.
	 * This should be called only if map is enabled.
	 * @private
	 */
	_initWidgetMiniActions() {
		// Mini widget hide
		const btnHide = this._createButton("gvs-mini-hide", this._fa(faAngleDown, {transform: {size: 24, rotate: 45, x: -3, y: 2}}), this._t.gvs.minimize, ["gvs-only-mini"]);
		this._corners["mini-bottom-left"].appendChild(btnHide);
		btnHide.addEventListener("click", () => {
			this._viewer.setUnfocusedVisible(false);
		});

		// Mini widget show
		const btnShow = this._createButton("gvs-mini-show", null, null, ["gvs-btn-large", "gvs-only-mini-hidden"]);
		this._corners["main-bottom-left"].appendChild(btnShow);
		btnShow.addEventListener("click", () => {
			this._viewer.setUnfocusedVisible(true);
		});

		const miniBtnRendering = () => {
			if(this._viewer.getMap() && this._viewer.isMapWide()) {
				btnShow.title = this._t.gvs.show_psv;
				btnShow.innerHTML = this._fat(faPanorama);
			}
			else {
				btnShow.title = this._t.gvs.show_map;
				btnShow.innerHTML = this._fat(faMap);
			}
		};

		miniBtnRendering();
		this._viewer.addEventListener("focus-changed", miniBtnRendering);
	}

	/**
	 * Creates share map/picture widget.
	 * @private
	 */
	_initWidgetShare() {
		const btnShare = this._createButton("gvs-share", this._fa(faShareFromSquare), this._t.gvs.share, ["gvs-btn-large"]);
		const pnlShare = this._createPanel(btnShare, []);
		pnlShare.innerHTML = `
			<h3>${this._fat(faShareFromSquare)} ${this._t.gvs.share}</h3>
			<div class="gvs-hidden" style="text-align: center">
				<p id="gvs-share-license" style="margin: 0 0 10px 0;"></p>
			</div>
			<h4>${this._fat(faLink)} ${this._t.gvs.share_links}</h4>
			<div id="gvs-share-links" class="gvs-input-btn" style="justify-content: center">
				${ this._viewer._options.hash ? `<button id="gvs-share-url" data-copy="true">${this._t.gvs.share_page}</button>`: ""}
				<a id="gvs-share-image" class="gvs-link-btn gvs-hidden" download target="_blank">${this._t.gvs.share_image}</a>
			</div>
			${ this._viewer._options.hash ? `
			<h4>${this._fat(faMap)} ${this._t.gvs.share_embed}</h4>
			<div class="gvs-input-btn">
				<textarea id="gvs-share-iframe" readonly></textarea>
				<button data-input="gvs-share-iframe">${this._t.gvs.copy}</button>
			</div>`: ""}
			<h4 class="gvs-hidden">${this._fat(faPen)} ${this._t.gvs.edit_osm}</h4>
			<div class="gvs-input-btn gvs-hidden" style="justify-content: center">
				${ this._viewer._options.hash ? `<a id="gvs-edit-id" class="gvs-link-btn" target="_blank">${this._t.gvs.id}</a>`: ""}
				<button id="gvs-edit-josm" title="${this._t.gvs.josm_live}">${this._fat(faSatelliteDish)} ${this._t.gvs.josm}</button>
			</div>
		`;
		this._createGroup("gvs-widget-share", "main-bottom-right", [btnShare, pnlShare], ["gvs-group-large", "gvs-group-btnpanel"]);

		// Add RSS link if available
		if(this._viewer._api.getRSSURL() && this._viewer._options.hash) {
			const grpLinks = document.getElementById("gvs-share-links");
			const btnRss = document.createElement("a");
			btnRss.id = "gvs-share-rss";
			btnRss.classList.add("gvs-link-btn");
			btnRss.setAttribute("target", "_blank");
			btnRss.setAttribute("title", this._t.gvs.share_rss_title);
			btnRss.appendChild(document.createTextNode(this._t.gvs.share_rss));
			grpLinks.appendChild(btnRss);
		}

		// Update picture download links
		this._viewer.addEventListener("picture-loaded", () => {
			const picMeta = this._viewer.getPictureMetadata();
			const hdLink = document.getElementById("gvs-share-image");
			hdLink.href = picMeta.panorama.hdUrl;

			const lblLicense = document.getElementById("gvs-share-license");
			lblLicense.innerHTML = picMeta?.caption?.license ? this._t.gvs.legend_license.replace("{l}", picMeta.caption.license) : "";

			while(pnlShare.getElementsByClassName("gvs-hidden").length > 0) {
				const h = pnlShare.getElementsByClassName("gvs-hidden")[0];
				h.classList.remove("gvs-hidden");
			}
		});

		// Update links
		const updateLinks = e => {
			const baseUrl = e?.detail?.url || window.location.href.replace(/\/$/, "");
			const fUrl = pnlShare.querySelector("#gvs-share-url");
			const fIframe = pnlShare.querySelector("#gvs-share-iframe");
			const btnId = pnlShare.querySelector("#gvs-edit-id");
			const btnRss = pnlShare.querySelector("#gvs-share-rss");

			fUrl.setAttribute("data-copy", baseUrl);
			fIframe.innerText = `
<iframe \n
	src="${baseUrl}"
	style="border: none; width: 500px; height: 300px"
></iframe>`;

			const meta = this._viewer.getPictureMetadata();
			if(meta) {
				btnId.setAttribute("href", `${this._options.editIdUrl}#map=19/${meta.gps[1]}/${meta.gps[0]}&source=Panoramax`);
			}

			if(btnRss) {
				btnRss.setAttribute("href", this._viewer._api.getRSSURL());
			}
		};

		if (this._viewer._options.hash) {
			updateLinks();
			this._viewer._hash.addEventListener("url-changed", updateLinks);
		}

		// Copy to clipboard on button click
		for(let btn of pnlShare.getElementsByTagName("button")) {
			const field = btn.getAttribute("data-input");
			const copy = btn.getAttribute("data-copy");
			if(field || copy) {
				btn.addEventListener("click", () => {
					let text;
					if(field) {
						const inputField = document.getElementById(field);
						text = inputField.innerText || inputField.value;
					}
					else if(copy) {
						text = btn.getAttribute("data-copy");
					}
					navigator.clipboard.writeText(text);
					const btnOrigContent = btn.innerHTML;
					btn.innerHTML = `${this._t.gvs.copied} ${this._fat(faCheck)}`;
					btn.classList.add("gvs-btn-active");
					setTimeout(() => {
						btn.innerHTML = btnOrigContent;
						btn.classList.remove("gvs-btn-active");
					}, 2000);
				});
			}
		}

		// JOSM live edit button
		const btnJosm = pnlShare.querySelector("#gvs-edit-josm");
		btnJosm.addEventListener("click", () => {
			// Disable
			if(btnJosm.classList.contains("gvs-btn-active")) {
				this._viewer.toggleJOSMLive(false);
			}
			// Enable
			else {
				this._viewer.toggleJOSMLive(true).catch(() => alert(this._t.gvs.error_josm));
			}
		});
		this._viewer.addEventListener("josm-live-enabled", () => btnJosm.classList.add("gvs-btn-active"));
		this._viewer.addEventListener("josm-live-disabled", () => btnJosm.classList.remove("gvs-btn-active"));
	}
}

import Viewer from "../Viewer";
import Widgets from "../Widgets";

jest.mock("../Viewer", () => (function (document, mapContainer) {
	const c = document.createElement("div");
	const c1 = document.createElement("div");
	const c2 = document.createElement("div");
	c.appendChild(c1);
	c.appendChild(c2);
	return {
		container: c,
		mainContainer: c1,
		miniContainer: c2,
		mapContainer,
		_translations: require("../translations.json").en,
		getPictureMetadata: jest.fn(),
		isSmall: jest.fn(),
		isHeightSmall: jest.fn(),
		psv: {
			getZoomLevel: jest.fn(),
		},
		addEventListener: jest.fn(),
		_hash: {
			addEventListener: jest.fn(),
		},
		_api: {
			getRSSURL: jest.fn(),
		},
	};
}));

describe("constructor", () => {
	it("works without map", () => {
		const d = document.createElement("div");
		document.body.appendChild(d);
		const v = new Viewer(document, d);
		new Widgets(v);
		expect(d).toMatchSnapshot();
	});

	it("handles custom widget (as string)", () => {
		const d = document.createElement("div");
		document.body.appendChild(d);
		const v = new Viewer(document, d);
		new Widgets(v, { customWidget: "<div id='bla'>BLA</div>" });
		expect(d).toMatchSnapshot();
	});

	it("handles custom widget (as DOM element)", () => {
		const d = document.createElement("div");
		document.body.appendChild(d);
		const v = new Viewer(document, d);
		const cw = document.createElement("a");
		cw.id = "blerg";
		new Widgets(v, { customWidget: cw });
		expect(d).toMatchSnapshot();
	});

	it("handles custom widget (as DOM elements)", () => {
		const d = document.createElement("div");
		document.body.appendChild(d);
		const v = new Viewer(document, d);
		const cw1 = document.createElement("a");
		cw1.id = "blerg";
		const cw2 = document.createElement("span");
		cw2.id = "pouf";
		new Widgets(v, { customWidget: [cw1, cw2] });
		expect(d).toMatchSnapshot();
	});
});

describe("destroy", () => {
	it("works", () => {
		const d = document.createElement("div");
		document.body.appendChild(d);
		const v = new Viewer(document, d);
		const w = new Widgets(v);
		w.destroy();
		expect(w._viewer).toBeUndefined();
		expect(w._t).toBeUndefined();
		expect(w._corners).toBeUndefined();
	});
});

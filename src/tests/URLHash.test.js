import Viewer from "../Viewer";
import URLHash from "../URLHash";

jest.mock("../Viewer", () => (function () {
	return {
		addEventListener: jest.fn(),
		removeEventListener: jest.fn(),
		goToPicture: jest.fn(),
		setXYZ: jest.fn(),
		getPictureMetadata: jest.fn(),
		getTransitionDuration: jest.fn(),
		setTransitionDuration: jest.fn(),
	};
}));

describe("constructor", () => {
	it("inits", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh).toBeDefined();
		expect(uh._viewer).toBe(v);
	});
});

describe("destroy", () => {
	it("works", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh).toBeDefined();
		expect(uh._viewer).toBe(v);
		uh.destroy();
		expect(uh._viewer).toBeUndefined();
		expect(v.removeEventListener.mock.calls.length).toBe(3);
	});
});


describe("getHashString", () => {
	it("works without any specific values set", () => {
		const v = new Viewer();
		v.getPictureMetadata = () => null;
		const uh = new URLHash(v);
		expect(uh.getHashString()).toBe("#");
	});

	it("works with picture metadata", () => {
		const v = new Viewer();
		v.getPictureMetadata = () => ({ "id": "cbfc3add-8173-4464-98c8-de2a43c6a50f" });
		const uh = new URLHash(v);
		uh._getXyzHashString = () => "0/1/2";
		expect(uh.getHashString()).toBe("#pic=cbfc3add-8173-4464-98c8-de2a43c6a50f&xyz=0/1/2");
	});

	it("works with picture wide", () => {
		const v = new Viewer();
		v.getPictureMetadata = () => ({ "id": "cbfc3add-8173-4464-98c8-de2a43c6a50f" });
		const uh = new URLHash(v);
		uh._getXyzHashString = () => "0/1/2";
		expect(uh.getHashString()).toBe("#pic=cbfc3add-8173-4464-98c8-de2a43c6a50f&xyz=0/1/2");
	});

	it("works with speed", () => {
		const v = new Viewer();
		v.getTransitionDuration = () => 250;
		const uh = new URLHash(v);
		expect(uh.getHashString()).toBe("#speed=250");
	});
});

describe("_getCurrentHash", () => {
	it("works if empty", () => {
		delete window.location;
		window.location = { hash: "" };
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh._getCurrentHash()).toStrictEqual({});
	});

	it("works with single param", () => {
		delete window.location;
		window.location = { hash: "#a=b" };
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh._getCurrentHash()).toStrictEqual({"a": "b"});
	});

	it("works with multiple params", () => {
		delete window.location;
		window.location = { hash: "#a=b&c=d" };
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh._getCurrentHash()).toStrictEqual({"a": "b", "c": "d"});
	});
});


describe("_getXyzHashString", () => {
	it("works", () => {
		const v = new Viewer();
		v.getXYZ = () => ({ x: 12, y: 50, z: 75 });
		const uh = new URLHash(v);
		expect(uh._getXyzHashString()).toBe("12.00/50.00/75");
	});

	it("rounds to 2 decimals", () => {
		const v = new Viewer();
		v.getXYZ = () => ({ x: 12.123456, y: 50.789456, z: 75 });
		const uh = new URLHash(v);
		expect(uh._getXyzHashString()).toBe("12.12/50.79/75");
	});

	it("works without z", () => {
		const v = new Viewer();
		v.getXYZ = () => ({ x: 12, y: 50 });
		const uh = new URLHash(v);
		expect(uh._getXyzHashString()).toBe("12.00/50.00/0");
	});
});

describe("getXyzOptionsFromHashString", () => {
	it("works", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh.getXyzOptionsFromHashString("18/-12.5/48.7")).toEqual({ x: 18, y: -12.5, z: 48.7 });
	});

	it("nulls if string is invalid", () => {
		const v = new Viewer();
		const uh = new URLHash(v);
		expect(uh.getXyzOptionsFromHashString("bla/bla/bla")).toBeNull();
	});
});


describe("_updateHash", () => {
	it("works", async () => {
		delete window.history;
		delete window.location;

		window.history = { replaceState: jest.fn(), state: {} };
		window.location = { href: "http://localhost:5000/#a=b&b=c" };

		const v = new Viewer();
		const uh = new URLHash(v);
		uh.getHashString = () => "#c=d";

		uh._updateHash();
		await new Promise((r) => setTimeout(r, 1000));

		expect(window.history.replaceState.mock.calls).toEqual([[{}, null, "http://localhost:5000/#c=d"]]);
	});

	it("deduplicates calls", async () => {
		delete window.history;
		delete window.location;

		window.history = { replaceState: jest.fn(), state: {} };
		window.location = { href: "http://localhost:5000/#a=b&b=c" };

		const v = new Viewer();
		const uh = new URLHash(v);
		uh.getHashString = () => "#c=d";

		for(let i=0; i <= 10; i++) {
			uh._updateHash();
		}

		await new Promise((r) => setTimeout(r, 1000));

		expect(window.history.replaceState.mock.calls).toEqual([[{}, null, "http://localhost:5000/#c=d"]]);
	});
});

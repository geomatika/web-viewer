import GVSViewer from "../Viewer";
import fs from "fs";
import path from "path";

jest.mock("@photo-sphere-viewer/core", () => ({
	Viewer: function() {
		return {
			getPlugin: (plugin) => new plugin(),
			addEventListener: jest.fn(),
			adapter: {
				queue: {
					tasks: []
				}
			},
			setPanorama: jest.fn(),
			autoSize: jest.fn(),
			startKeyboardControl: jest.fn(),
			stopKeyboardControl: jest.fn(),
			loader: {
				show: jest.fn(),
				canvas: {
					setAttribute: jest.fn(),
				},
				__updateContent: jest.fn(),
			},
			renderer: {
				renderer: {
					toneMapping: 0,
					toneMappingExposure: 1,
				},
			},
			destroy: jest.fn(),
		};
	},
	DEFAULTS: {
		keyboardActions: {},
	},
}));

jest.mock("@photo-sphere-viewer/equirectangular-tiles-adapter", () => ({
	EquirectangularTilesAdapter: jest.fn(),
}));

jest.mock("@photo-sphere-viewer/virtual-tour-plugin", () => ({
	VirtualTourPlugin: function() {
		return {
			addEventListener: jest.fn(),
			viewer: {
				setPanorama: jest.fn(),
			},
			__onEnterObject: jest.fn(),
			__onLeaveObject: jest.fn(),
		};
	}
}));

const API_URL = "http://localhost:5000/api/search";

describe("constructor", () => {
	it("inits", () => {
		const d = document.createElement("div");
		const v = new GVSViewer(d, API_URL, { testing: true });
		expect(v).toBeDefined();
	});
});


describe("destroy", () => {
	it("works", () => {
		const d = document.createElement("div");
		d.id = "geovisio";
		document.body.appendChild(d);
		const v = new GVSViewer(d, API_URL, { testing: true });
		v._initContainerStructure("geovisio");
		v._hash = { destroy: jest.fn() };
		v._widgets = { destroy: jest.fn() };
		v.psv = { destroy: jest.fn() };

		v.destroy();

		expect(v._api).toBeUndefined();
		expect(v._hash).toBeUndefined();
		expect(v.psv).toBeUndefined();
		expect(d.innerHTML).toBe("");
	});
});

describe("_initContainerStructure", () => {
	beforeEach(() => {
		document.body.innerHTML = "";
	});

	it("works with string", () => {
		const d = document.createElement("div");
		d.id = "geovisio";
		document.body.appendChild(d);
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._initContainerStructure("geovisio");
		expect([...d.children].find(n => n.className.includes("gvs-main"))).toBeDefined();
	});

	it("works with Element", () => {
		const d = document.createElement("div");
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._initContainerStructure(d);
		expect([...d.children].find(n => n.className.includes("gvs-main"))).toBeDefined();
	});

	it("fails on missing element", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		expect(() => v._initContainerStructure("geovisio")).toThrow(new Error("Container is not a valid HTML element, does it exist in your page ?"));
	});
});

describe("_initPSV", () => {
	it("works", () => {
		const d = document.createElement("div");
		const v = new GVSViewer(d, API_URL, { testing: true });
		v._initContainerStructure(d);
		v._initPSV();
		expect(v.psv).toBeDefined();
	});
});

describe("_getNodeFromAPI", () => {
	beforeEach(() => {
		jest.clearAllMocks();
	});

	it("works", async () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._api = { getPictureMetadataUrl: () => "", _getFetchOptions: () => ({}) };
		global.fetch = jest.fn(() =>
			Promise.resolve({
				json: () => Promise.resolve(JSON.parse(fs.readFileSync(path.join(__dirname, "data", "Viewer_pictures_1.json")))),
			})
		);
		global.Date = jest.fn(() => ({ toLocaleDateString: () => "June 3 2022" }));
		const res = await v._getNodeFromAPI("id");
		expect(res).toMatchSnapshot();
	});
});

describe("_getNodeCaption", () => {
	it("works with date", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		const m = { properties: { datetime: "2022-02-01T12:15:36Z" } };
		global.Date = jest.fn(() => ({ toLocaleDateString: () => "February 2 2022" }));
		const res = v._getNodeCaption(m);
		expect(res).toMatchSnapshot();
	});

	it("works with producer", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		const m = { providers: [ { name: "PanierAvide", roles: ["producer", "licensor"] } ] };
		const res = v._getNodeCaption(m);
		expect(res).toMatchSnapshot();
	});

	it("works with date + producer", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		const m = { properties: { datetime: "2022-02-01T12:15:36Z" }, providers: [ { name: "PanierAvide", roles: ["producer", "licensor"] } ] };
		global.Date = jest.fn(() => ({ toLocaleDateString: () => "February 2 2022" }));
		const res = v._getNodeCaption(m);
		expect(res).toMatchSnapshot();
	});

	it("works with date + 2 producers", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		const m = {
			properties: { datetime: "2022-02-01T12:15:36Z" },
			providers: [
				{ name: "GeoVisio Corp.", roles: ["producer", "licensor"] },
				{ name: "PanierAvide", roles: ["producer"] },
			]
		};
		global.Date = jest.fn(() => ({ toLocaleDateString: () => "February 2 2022" }));
		const res = v._getNodeCaption(m);
		expect(res).toMatchSnapshot();
	});
});

describe("_positionToXYZ", () => {
	it("works with xy", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		const r = v._positionToXYZ({ pitch: 10, yaw: -5 });
		expect(r).toEqual({ x: -286.4788975654116, y: 572.9577951308232 });
	});

	it("works with xyz", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		const r = v._positionToXYZ({ pitch: 10, yaw: -5 }, 15);
		expect(r).toEqual({ x: -286.4788975654116, y: 572.9577951308232, z: 15 });
	});
});

describe("_xyzToPosition", () => {
	it("works with xyz", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		const r = v._xyzToPosition(-286.4788975654116, 572.9577951308232, 15);
		expect(r).toEqual({ pitch: 10, yaw: -5, zoom: 15 });
	});
});

describe("_onTilesStartLoading", () => {
	it("fires event when tiles queue clears", () => {
		const d = document.createElement("div");
		const v = new GVSViewer(d, API_URL, { testing: true });
		v._initContainerStructure(d);
		v._initPSV();
		v._myVTour = { state: { currentNode: { id: "1" } } };

		v.psv.adapter.queue.tasks = ["1", "2", "3"];

		return expect(new Promise(resolve => {
			v._onTilesStartLoading();
			v.addEventListener("picture-tiles-loaded", resolve);
			v.psv.adapter.queue.tasks = [];
		})).resolves.toEqual(new Event("picture-tiles-loaded", { picId: "1" }));
	});
});

describe("_onNextPrevPicClick", () => {
	it("works for prev click", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.getPictureMetadata = () => ({ sequence: { "prevPic": "x" } });
		v.goToPrevPicture = jest.fn();
		v.goToNextPicture = jest.fn();
		v._onNextPrevPicClick("prev");
		expect(v.goToPrevPicture.mock.calls).toEqual([[]]);
		expect(v.goToNextPicture.mock.calls.length).toEqual(0);
	});

	it("works for next click", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.getPictureMetadata = () => ({ sequence: { "nextPic": "x" } });
		v.goToPrevPicture = jest.fn();
		v.goToNextPicture = jest.fn();
		v._onNextPrevPicClick("next");
		expect(v.goToPrevPicture.mock.calls.length).toEqual(0);
		expect(v.goToNextPicture.mock.calls).toEqual([[]]);
	});
});

describe("getXY", () => {
	it("works", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.psv = { getPosition: () => ({ yaw: 0.7853981634, pitch: -1.2217304764 }) };
		const r = v.getXY();
		expect(r).toEqual({ x: 45.0000000001462, y: -70.00000000022743 });
	});
});

describe("getXYZ", () => {
	it("works", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.psv = {
			getPosition: () => ({ yaw: 0.7853981634, pitch: -1.2217304764 }),
			getZoomLevel: () => 15
		};
		const r = v.getXYZ();
		expect(r).toEqual({ x: 45.0000000001462, y: -70.00000000022743, z: 15 });
	});
});

describe("getPictureMetadata", () => {
	it("works", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._myVTour = { state: { currentNode: { "id": "1" } } };
		expect(v.getPictureMetadata()).toEqual({ "id": "1" });
	});

	it("is null if no current node", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._myVTour = { state: { currentNode: null } };
		expect(v.getPictureMetadata()).toBeNull();
	});
});

describe("getPhotoViewer", () => {
	it("works", () => {
		const d = document.createElement("div");
		const v = new GVSViewer(d, API_URL, { testing: true });
		v._initContainerStructure(d);
		v._initPSV();
		expect(v.getPhotoViewer()).toEqual(v.psv);
	});
});

describe("goToPicture", () => {
	it("works", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._initPSV();
		v._myVTour = { setCurrentNode: jest.fn(), getCurrentNode: jest.fn() };
		v.goToPicture("id");
		expect(v._myVTour.setCurrentNode.mock.calls).toEqual([["id"]]);
	});

	it("works on pic ID already used", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._initPSV();
		v._myVTour = { setCurrentNode: jest.fn(), getCurrentNode: () => "id" };
		v.goToPicture("id");
		expect(v._myVTour.setCurrentNode.mock.calls).toEqual([["id"]]);
	});
});

describe("playSequence", () => {
	it("sends event", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		return expect(new Promise(resolve => {
			v.addEventListener("sequence-playing", resolve);
			v.playSequence();
		})).resolves.toBeDefined();
	});
});

describe("stopSequence", () => {
	it("sends event", async () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		return expect(new Promise(resolve => {
			v.addEventListener("sequence-stopped", resolve);
			v.stopSequence();
		})).resolves.toBeDefined();
	});
});

describe("isSequencePlaying", () => {
	it("is true when sequence is playing", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.psv = { addEventListener: jest.fn() };
		v.goToNextPicture = jest.fn();
		v.playSequence();
		expect(v.isSequencePlaying()).toBe(true);
	});

	it("is false when sequence never played", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.psv = { addEventListener: jest.fn() };
		expect(v.isSequencePlaying()).toBe(false);
	});

	it("is false when sequence stopped", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.psv = { addEventListener: jest.fn() };
		v.playSequence();
		v.stopSequence();
		expect(v.isSequencePlaying()).toBe(false);
	});
});

describe("goToNextPicture", () => {
	it("fails if no current picture", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._myVTour = { state: { currentNode: undefined } };
		expect(() => v.goToNextPicture()).toThrow(new Error("No picture currently selected"));
	});

	it("works if next pic exists", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._myVTour = { state: { currentNode: { sequence: { nextPic: "idnext" } } } };
		v.goToPicture = jest.fn();
		v.goToNextPicture();
		expect(v.goToPicture.mock.calls).toEqual([["idnext"]]);
	});

	it("fails if no next picture", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._myVTour = { state: { currentNode: { sequence: {} } } };
		expect(() => v.goToNextPicture()).toThrow(new Error("No next picture available"));
	});
});

describe("goToPrevPicture", () => {
	it("fails if no current picture", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._myVTour = { state: { currentNode: undefined } };
		expect(() => v.goToPrevPicture()).toThrow(new Error("No picture currently selected"));
	});

	it("works if next pic exists", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._myVTour = { state: { currentNode: { sequence: { prevPic: "idprev" } } } };
		v.goToPicture = jest.fn();
		v.goToPrevPicture();
		expect(v.goToPicture.mock.calls).toEqual([["idprev"]]);
	});

	it("fails if no next picture", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._myVTour = { state: { currentNode: { sequence: {} } } };
		expect(() => v.goToPrevPicture()).toThrow(new Error("No previous picture available"));
	});
});

describe("goToPosition", () => {
	it("works", async () => {
		global.fetch = jest.fn(() =>
			Promise.resolve({
				json: () => Promise.resolve(JSON.parse(fs.readFileSync(path.join(__dirname, "data", "Viewer_pictures_1.json")))),
			})
		);

		const v = new GVSViewer(null, API_URL, { testing: true });
		v._api = { getPicturesAroundCoordinatesUrl: jest.fn(), _getFetchOptions: jest.fn() };
		v.goToPicture = jest.fn();

		const res = await v.goToPosition(48.7, -1.8);

		expect(res).toEqual("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(v.goToPicture.mock.calls).toEqual([["0005086d-65eb-4a90-9764-86b3661aaa77"]]);
	});

	it("handles empty result from API", () => {
		global.fetch = jest.fn(() =>
			Promise.resolve({
				json: () => Promise.resolve({ "features": [] }),
			})
		);

		const v = new GVSViewer(null, API_URL, { testing: true });
		v._api = { getPicturesAroundCoordinatesUrl: jest.fn(), _getFetchOptions: jest.fn() };
		return expect(v.goToPosition()).rejects.toStrictEqual(new Error("No picture found nearby given coordinates"));
	});
});

describe("setXYZ", () => {
	it("works", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.psv = { rotate: jest.fn(), zoom: jest.fn() };
		v.setXYZ(45, -45, 3);

		expect(v.psv.zoom.mock.calls).toEqual([[3]]);
		expect(v.psv.rotate.mock.calls).toEqual([[{ yaw: 0.7853981633974483, pitch: -0.7853981633974483 }]]);
	});
});

describe("getTransitionDuration", () => {
	it("works", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v._transitionDuration = 350;
		expect(v.getTransitionDuration()).toBe(350);
	});
});

describe("setTransitionDuration", () => {
	it("works", () => {
		const v = new GVSViewer(null, API_URL, { testing: true });
		v.setTransitionDuration(400);
		expect(v.getTransitionDuration()).toBe(400);
	});
});
